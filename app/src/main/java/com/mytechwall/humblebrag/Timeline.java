package com.mytechwall.humblebrag;

import android.app.ListActivity;
import android.os.Bundle;

import com.twitter.sdk.android.tweetui.SearchTimeline;
import com.twitter.sdk.android.tweetui.TweetTimelineListAdapter;

public class Timeline extends ListActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String s = getIntent().getExtras().getString("search");

        final SearchTimeline searchTimeline = new SearchTimeline.Builder()
                .query("#" + s)
                .build();
        final TweetTimelineListAdapter adapter = new TweetTimelineListAdapter.Builder(this)
                .setTimeline(searchTimeline)
                .build();

        setListAdapter(adapter);


    }
}
