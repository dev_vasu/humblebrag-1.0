package com.mytechwall.humblebrag;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterCore;

import io.fabric.sdk.android.Fabric;

public class Splash extends AppCompatActivity {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "C8yMsKHKr0onHxNhDvr2J3Gy6";
    private static final String TWITTER_SECRET = "LPfHXO8UIoUUZTtRNL5T8reJCVMnprRcEcdgzxcwzc9u18SyHg";
    TextView title, subtitle;
    ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));
        setContentView(R.layout.activity_splash);
        title = (TextView) findViewById(R.id.splash_head);
        subtitle = (TextView) findViewById(R.id.splash_subtitle);
        image = (ImageView) findViewById(R.id.splash_image);


        YoYo.with(Techniques.ZoomInDown).duration(1000).playOn(image);
        YoYo.with(Techniques.SlideInUp).duration(1000).playOn(title);
        YoYo.with(Techniques.SlideInUp).duration(1000).playOn(subtitle);

        final boolean connected = isNetworkAvailable();

        if (!connected) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Connect to internet and restart app")
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            finish();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        }



        Thread timer = new Thread() {
            @Override
            public void run() {
                try {
                    sleep(2500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {

                    if (connected) {

                        if (TwitterCore.getInstance().getSessionManager().getActiveSession() != null) {
                            Intent i = new Intent(getApplicationContext(), MainScreen.class);


                            startActivity(i);
                            finish();
                        } else {
                            Intent i = new Intent(getApplicationContext(), LoginActivity.class);


                            startActivity(i);
                            finish();
                        }
                    }


                }
            }

        };
        timer.start();

    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
