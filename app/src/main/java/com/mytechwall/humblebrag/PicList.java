package com.mytechwall.humblebrag;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.twitter.sdk.android.core.models.Tweet;
import com.twitter.sdk.android.tweetui.SearchTimeline;
import com.twitter.sdk.android.tweetui.TweetTimelineListAdapter;

import java.util.ArrayList;

public class PicList extends AppCompatActivity {

    ArrayList<String> imageURLs;
    ArrayList<String> names;
    int count = 0;
    int j = 0;

    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pic_list);

        recyclerView = (RecyclerView) findViewById(R.id.card_recycler_view);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);

        imageURLs = new ArrayList<>();
        names = new ArrayList<>();

        final SearchTimeline searchTimeline = new SearchTimeline.Builder()
                .query("#humblebrag")
                .maxItemsPerRequest(100)
                .build();
        final TweetTimelineListAdapter adapter = new TweetTimelineListAdapter.Builder(this)
                .setTimeline(searchTimeline)
                .build();
        final ProgressDialog progressBar = new ProgressDialog(this);
        progressBar.setCancelable(false);
        progressBar.setMessage("Downloading ...");
        progressBar.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressBar.setProgress(0);
        progressBar.setMax(100);
        progressBar.show();
        initViews();
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(PicList.this, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        // TODO Handle item click
                        Toast.makeText(PicList.this, " " + names.get(position), Toast.LENGTH_SHORT).show();
                        Intent i = new Intent(PicList.this, PhotoViewer.class);

                        Bundle b = new Bundle();
                        b.putStringArray("urls", imageURLs.toArray(new String[0]));
                        b.putInt("position", position);
                        i.putExtras(b);
                        startActivity(i);

                    }
                })
        );


        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted() && (count < 100)) {
                        Thread.sleep(100);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (count < adapter.getCount()) {

                                    Tweet itemAtPosition = (Tweet) adapter.getItem(count);
                                    imageURLs.add(itemAtPosition.user.profileImageUrlHttps);
                                    names.add(itemAtPosition.user.name);
                                    count++;

                                    progressBar.setProgress(count);
                                    //adapter.notifyDataSetChanged();
                                    initViews();
                                }
                                if (count == 100) {
                                    progressBar.dismiss();

                                }
                            }
                        });

                    }
                } catch (InterruptedException e) {
                }
            }
        };
        t.start();

    }

    private void initViews() {

        ArrayList androidVersions = prepareData();
        DataAdapter adapter = new DataAdapter(getApplicationContext(), androidVersions);
        recyclerView.setAdapter(adapter);

    }

    private ArrayList prepareData() {

        ArrayList single_detail = new ArrayList<>();
        for (int i = 0; i < names.size(); i++) {
            SingleDetail single = new SingleDetail();
            single.setName_of_person(names.get(i));
            single.setImage_of_person(imageURLs.get(i));
            single_detail.add(single);
        }
        return single_detail;
    }
}
