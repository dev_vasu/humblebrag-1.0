package com.mytechwall.humblebrag;

/**
 * Created by Vasu on 9/15/2016.
 */
public class SingleDetail {

    private String image_of_person;
    private String name_of_person;

    public String getImage_of_person() {
        return image_of_person;
    }

    public void setImage_of_person(String image_of_person) {
        this.image_of_person = image_of_person;
    }

    public String getName_of_person() {
        return name_of_person;
    }

    public void setName_of_person(String name_of_person) {
        this.name_of_person = name_of_person;
    }
}
