package com.mytechwall.humblebrag;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder> {
    private ArrayList<SingleDetail> singleDetails;
    private Context context;

    public DataAdapter(Context context, ArrayList<SingleDetail> singleDetails) {
        this.context = context;
        this.singleDetails = singleDetails;

    }

    @Override
    public DataAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_layout, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {


        viewHolder.name.setText(singleDetails.get(i).getName_of_person());
        Picasso.with(context).load(singleDetails.get(i).getImage_of_person()).resize(50, 50).into(viewHolder.image);
    }

    @Override
    public int getItemCount() {
        return singleDetails.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        ImageView image;

        public ViewHolder(View view) {
            super(view);

            name = (TextView) view.findViewById(R.id.name_of_person);
            image = (ImageView) view.findViewById(R.id.image_of_person);
        }
    }
}