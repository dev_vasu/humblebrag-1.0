package com.mytechwall.humblebrag;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.models.Tweet;
import com.twitter.sdk.android.tweetcomposer.ComposerActivity;

public class MainScreen extends AppCompatActivity {

    FloatingActionButton fab;
    Button timeline;
    Button onlyPics;
    Button tweet;
    ImageView mainImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_main_screen);
        timeline = (Button) findViewById(R.id.button_timeline);
        fab = (FloatingActionButton) findViewById(R.id.floating_search);
        onlyPics = (Button) findViewById(R.id.button_only_pics);
        tweet = (Button) findViewById(R.id.button_tweet);
        mainImage = (ImageView) findViewById(R.id.main_image);

        String user = TwitterCore.getInstance().getSessionManager().getActiveSession().getUserName();
        getSupportActionBar().setTitle("Welcome " + user);

        tweet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YoYo.with(Techniques.BounceIn).duration(100).playOn(tweet);

                final TwitterSession session = TwitterCore.getInstance().getSessionManager()
                        .getActiveSession();
                final Intent intent = new ComposerActivity.Builder(MainScreen.this)
                        .session(session)
                        .createIntent();
                startActivity(intent);

            }
        });

        timeline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YoYo.with(Techniques.BounceIn).duration(100).playOn(timeline);

                Intent i = new Intent(MainScreen.this, Timeline.class);
                i.putExtra("search", "humblebrag");
                startActivity(i);

            }
        });



        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                               pulseIt();
                            }
                        });

                    }
                } catch (InterruptedException e) {
                }
            }
        };
        t.start();





        fab.setOnClickListener(new View.OnClickListener() {
                                   @Override
                                   public void onClick(View view) {

                                       String search = "";
                                       YoYo.with(Techniques.BounceIn).duration(100).playOn(fab);

                                       LayoutInflater li = LayoutInflater.from(MainScreen.this);
                                       View promptsView = li.inflate(R.layout.prompts, null);

                                       AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                                               MainScreen.this);

                                       // set prompts.xml to alertdialog builder
                                       alertDialogBuilder.setView(promptsView);

                                       final EditText userInput = (EditText) promptsView
                                               .findViewById(R.id.editTextDialogUserInput);

                                       // set dialog message
                                       alertDialogBuilder
                                               .setCancelable(false)
                                               .setPositiveButton("OK",
                                                       new DialogInterface.OnClickListener() {
                                                           public void onClick(DialogInterface dialog, int id) {
                                                               // get user input and set it to result
                                                               // edit text

                                                               if (!(userInput.getText() == null)) {
                                                                   if (userInput.getText().length() != 0) {
                                                                       String search = userInput.getText().toString();
                                                                       Toast.makeText(MainScreen.this, "Searcing for " + search, Toast.LENGTH_SHORT).show();
                                                                       Intent i = new Intent(MainScreen.this, Timeline.class);
                                                                       i.putExtra("search", search);
                                                                       startActivity(i);
                                                                   } else {
                                                                       Toast.makeText(MainScreen.this, "Enter some text", Toast.LENGTH_SHORT).show();
                                                                   }
                                                               }
                                                           }
                                                       })
                                               .setNegativeButton("Cancel",
                                                       new DialogInterface.OnClickListener() {
                                                           public void onClick(DialogInterface dialog, int id) {
                                                               Toast.makeText(MainScreen.this, "Maybe next time", Toast.LENGTH_SHORT).show();
                                                               dialog.cancel();
                                                           }
                                                       });

                                       // create alert dialog
                                       AlertDialog alertDialog = alertDialogBuilder.create();
                                       // show it
                                       alertDialog.show();
                                       userInput.requestFocus();

                                   }


                               }

        );
        onlyPics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YoYo.with(Techniques.BounceIn).duration(100).playOn(onlyPics);

                startActivity(new Intent(MainScreen.this, PicList.class));
            }
        });
    }

    private void pulseIt() {
        YoYo.with(Techniques.Pulse).duration(900).playOn(mainImage);
        YoYo.with(Techniques.Wave).duration(500).playOn(fab);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.logout:
                // Single menu item is selected do something
                // Ex: launching new activity/screen or show alert message
                TwitterCore.getInstance().getSessionManager().clearActiveSession();
                startActivity(new Intent(MainScreen.this, LoginActivity.class));
                finish();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }
}